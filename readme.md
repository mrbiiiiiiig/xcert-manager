# XShell配置文件生成工具

## 简介

- 仅适用于Windows平台
- 暂时只用于生成XShell的配置文件
- 目前仅`6.X`、`7.X`版本实测通过
- 需要`Python 3.6+`

## 依赖

- openpyxl
- pywin32
- pycryptodome
- ipaddress

## 使用方法

1. ### 安装依赖

    ``` shell
    pip install --trusted-host mirrors.aliyun.com -i http://mirrors.aliyun.com/pypi/simple -r requirements
    ```
2. ### 修改main.py中相关配置

    ``` python
   # 服务器配置EXCEL文件
   source_file
   # 工作表名称集合
   sheet_names
   # 生成的配置文件存放位置（不指定则生成到XShell默认配置文件目录）
   target_dir
   # XShell版本（在XShell中任意创建SSH配置文件，以文本方式打开，查找其中Version字段的值）
   xshell_version
   # 跳板机配置
   using_jumpserver
   jump_server_conf
   ```

3. ### 运行程序

    ``` shell
    python main.py
    ```
