#!/usr/bin/env python3
from base64 import b64encode, b64decode
from Crypto.Hash import MD5, SHA256
from Crypto.Cipher import ARC4

passward = 'Best1&^%Red6#@123'
user = 'Admin'
fu = 'bej-nb-2528\admin'
sid = 'S-1-5-21-2041162310-3622943907-3722255300-500'
group = 'Administrators'
gid = 'S-1-5-32-544'
domain = 'BEJ-NB-2528'
wifimac = '68-54-5A-C5-87-BC'
lanmac = '74-78-27-2D-A9-58'
# 目标格式6         VHrd3yDwp/TG3pOy7/moC8nvBDlFq9ly+LVBccyZhgYcXpMOZZ4LFKFNvqzaRHMdug==
# 目标格式7         8YboxqcwUzwV36bIIXWDV9fvBDlFq9ly+LVBccyZhgYcXpMOZZ4LFKFNvqzaRHMdug==
# version < 5.1  将"!X@s#h$e%l^l&"的MD5值作为RC4的密钥key
# version == 5.1 or 5.2 将计算机用户的SID序列号的SHA-256值作为RC4的密钥key
# version > 5.2 将计算机的用户名+SID序列号的SHA-256值作为RC4的密钥key
# version = 7.1 将计算机的用户名+SID倒序的字符串的SHA-256值作为RC4的密钥key
assemStr =  user + sid
# assemStr =  sid[::-1] + user
print(assemStr)
Cipher = ARC4.new(SHA256.new((assemStr).encode()).digest())
checksum = SHA256.new(passward.encode()).digest()
ciphertext = Cipher.encrypt(passward.encode())
print('####################################')
print(b64encode(ciphertext + checksum).decode())
