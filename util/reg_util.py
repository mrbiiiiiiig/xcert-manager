#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import winreg


def get_user_folder_path(value_name: str) -> str:
    """
    获取当前用户目录路径（文档、桌面等目录）
    """
    key_path = r'Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders'
    with winreg.OpenKey(winreg.HKEY_CURRENT_USER, key_path, 0, winreg.KEY_READ) as reg_key:
        value, value_type = winreg.QueryValueEx(reg_key, value_name)
    return value
