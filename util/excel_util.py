#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from openpyxl import Workbook, load_workbook
from openpyxl.worksheet.worksheet import Worksheet


class XConfColumnStrMapping(object):
    """
    XShell/XFtp配置项对应Excel列的映射，列索引从0开始（A列为0）
    """

    def __init__(self,
                 host: str,
                 port: str,
                 username: str,
                 password: str,
                 role: str,
                 cluster_id: str,
                 app_dir: str,
                 cd_app_dir: str,
                 conf_sub_dir: str,
                 remark: str):
        """
        :param host: 主机/IP地址
        :param port: 端口
        :param username: 用户名
        :param password: 密码
        :param role: 应用角色
        :param cluster_id: 集群ID
        :param app_dir: 应用目录
        :param cd_app_dir: （连接时）自动进入应用目录
        :param conf_sub_dir: （生成的）配置文件子目录
        :param remark: 备注
        """
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.role = role
        self.cluster_id = cluster_id
        self.app_dir = app_dir
        self.cd_app_dir = cd_app_dir
        self.conf_sub_dir = conf_sub_dir
        self.remark = remark


def read_sheets_from_excel(file_path: str, sheet_names: list) -> list:
    """
    从EXCEL文件加载工作表集合
    :param file_path: EXCEL文件路径
    :param sheet_names: 工作表名称集合
    :return: 工作表集合
    """
    if file_path is None or sheet_names is None or len(sheet_names) < 1:
        raise RuntimeError('文件路径名、工作表名集合不能为空')
    # 载入Excel文件
    workbook: Workbook = load_workbook(file_path, True)
    sheets = []
    for sheet_name in sheet_names:
        sheet: Worksheet = workbook[sheet_name]
        if sheet is None:
            raise RuntimeError(f'Excel文件中没有名称为“{sheet_name}”的表')
        sheets.append(sheet)
    return sheets
